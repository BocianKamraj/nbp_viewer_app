﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBP_viewer
{
    class DataList
    {
        public DataList()
        {
            CountryList = new ObservableCollection<Country>();
            CountryList.Add(new Country() { Name = "Polska" });
            CountryList.Add(new Country() { Name = "Azerbejdzanloliksde" });
        }

        public ObservableCollection<Country> CountryList { get; set; }
        
        public void AddCountry(string CountryName)
        {
            CountryList.Add(new Country { Name = CountryName });
        }

        public ObservableCollection<Country> Items{
        get => CountryList;
        }
    }
}